
# Building your first EdgeNet app - part 2

Welcome to the second part of our tutorial on building your first EdgeNet app. In this part, we will expand the short example site from last time into a bit more fleshed-out example app.

```

class VideoPlayerJS extends React.Component {
  constructor(props) {
    super(props);
    this.state = {buffered: 0};
  }

  componentDidMount() {
    // instantiate Video.js
    this.player = videojs(this.videoNode, this.props, function onPlayerReady() {
      console.log('onPlayerReady', this)
    });
      this.timerID = setInterval(() => this.tick(), 500  );
  }
  //
  tick() {
    if (this.player) {
      this.setState({buffered: this.player.bufferedPercent()});
    }
  }

  // destroy player on unmount
  componentWillUnmount() {
    if (this.player) {
      this.player.dispose()
    }
    clearInterval(this.timerID);
  }

  // wrap the player in a div with a `data-vjs-player` attribute
  // so videojs won't create additional wrapper in the DOM
  // see https://github.com/videojs/video.js/pull/3856
  render() {
    return (
      <div>	
        <div data-vjs-player>
          <video ref={ node => this.videoNode = node } className="video-js vjs-theme-forest"></video>
        </div>
          <div> {this.state ? this.state.buffered : "0"} % </div>
      </div>
    )
  }
}



```
