# EdgeNet Video Enablement Quickstart

Working with new APIs is exciting. In our EdgeNet partner program we are among the first to utilize the Alef Video Enablement API. The API gives you the ability to stream video at high speeds from an edge location to an edge-enabled device. Since your device is not (yet) edge-enabled, we have to work through a Sandbox abstraction in order to simulate the edge device.

The following diagram helps you understand the needed architecture for developing apps that utilize the API.

![Diagram](https://lh4.googleusercontent.com/YzqhTxzI8sV_lcwMMxbFEA2X5ogDD7X_6-v0sc3J0fHZRYk8AbAvmuMLKANg47EHzcJq3BGNwZqYT68Znlg0siF6wEJZr4fX_GnYZGe-RV-QRTg-M8tmCW2ONXRj_Qso53zxQi1H)

However, we want to develop apps that can also be used on regular browsers, as well as mobile apps. Thus, the current recommended practise for competing in the EdgeNet challenges is to include the following kind of code snippet as the logic for your video playback methods:

```
if (edge URL loads successfully) {
  load(videoFromEdgeUrl);
} else {
  load(videoFromCloudUrl);
}
```

If the app is accessed from an edge-enabled device, you will be able to fully utilize the EdgeNet API and connectivity, but the app will also remain usable and testable by reviewers when using ordinary methods.

The main piece of official documentation for this API is available here: https://developer.alefedge.com/reference-docs/edgenet-developer-portal/service-usage-docs/api-usage-5g-video-enablement/ and here: https://developer.alefedge.com/get-started/play-your-first-edge-video/5g-video-streaming-tutorial/

For using the API, I recommend starting with their Swagger site: https://developerapis.stg-alefedge.com/api-docs-edgetube/ 
You can copy your API key and Service ID details into the Swagger web form and fill in the required JSON values to make the necessary calls.

Once you get the hang of it, it should be easy to program the necessary calls into your solution as well. 


## Main sandbox workflow
To make it a bit more clear, here is a quick overline of the usual workflow when working with this API with the full sandbox.

The workflow is as follows:
(Prerequisite: have a Video Enablement Service for your account)
(Prerequisite: have the video you need already hosted somewhere)
* Call /add API endpoint with both "url" and "partner_cloud_url" set to the url pointing to your video. Set both "content_access" and "publish_access" to value "public"
* Use /get-all API to get status and edge url
* if "content_upload_status" is "success", you can access the video through the edge url represented by "content_url" value (but only in a sandbox)
* Provision sandbox at the same location as your service and view your video

In the mobile challenges, you are asked to make your app work without the last step above, and to use the original Cloud URL as the fallback content location. Local storage is also recommended as an alternative if possible. If you write for desktop, then the app should still work through the sandbox as well. See the pseudocode above for the logic.

When working with HLS media and web apps, you must also take note to additionally add the URL to your app as a whitelist location through the API, by calling the /add-whitelist-domain API endpoint. This is necessary for working with CORS security related details in the browser.

## Previous competitions
See the writeup by challenge winner [[greenmug](https://www.topcoder.com/members/greenmug)] for his approach in writing a mobile app for the EdgeNet Video News Feeds App challenge here: https://www.topcoder.com/thrive/articles/edgenet-challenge-series-video-news-feeds-app-solution-writeup

## References & further reading

Topcoder Edgenet community program https://www.topcoder.com/community/member-programs/edgenet?tracks[edgenet-compete]=1

Alef https://alefedge.com/

Alef EdgeNet https://alefedge.com/alef-edgenet/

Alef Video Enablement API https://alefedge.com/products/prepackaged-solutions/alef-video-enablement-1/

Wikipedia, Edge computing, https://en.wikipedia.org/wiki/Edge_computing

Wikipedia, 5G https://en.wikipedia.org/wiki/5G

Wikipedia, Internet of things, https://en.wikipedia.org/wiki/Internet_of_things

