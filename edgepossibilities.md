

# Edge possibilities

Welcome to our latest post on the Alef EdgeNet partnership program. In this article, we will go through some possible use cases that can be implemented with the Alef EdgeNet APIs. We'll be mainly looking at the Video Enablement API and the Video Notifications API this time.

The Alef Video Enablement API enables you to stream video from edge locations to your users at high speeds. This is enabled by the use of edge computing within the backing infrastructure. The API allows you to upload content to the edge, and to stream it from the edge to your users. Both static and streaming media are supported by the API.

## Use Case 1: Vanilla video player

Edge computing represents the future and it is worthwhile to upgrade your ordinary video player apps to be edge-enabled. You can do this as an upgrade process to bring additional features to your existing cloud apps without having to sacrifice functionality on the way, or to take a gamble for new tech. You will simply gain the benefits of edge computing in addition to your existing functionality.

Bringing computation and content closer to the client through the edge allows you to also bring localisation more heavily into play for your products. You can bring the most valuable and relevant content to your audience at ultra-high speeds and bandwidth. This means target language content, target sector content, interest area content, and valuable partnership content at edge-enabled ultra-high speeds. Less relevant content can still be supported as before. Bringing in more personalisation and localisation to your apps allows your product to be more relevant to your audience and thus bring in increased user satisfaction.

## Educational portal

Expanding on the above, a use case that would enjoy definite benefits from an upgrade to the edge-enabled future is an educational web portal. This can be an internal portal for your company communications, or a full educational product that you are building for your audience. Edge represents much higher user satisfaction and experience for your portal by bringing the relevant content close to the consumers. Being able to stream all relevant content at high speeds will bring you the edge you need to win over your viewers and keep them engaged.

### Use case 3: Company intranet portal

For a company internal resource, you will be able to localize office location -specific content right to the relevant office location. Your employees will be able to enjoy onboarding video tutorials, role-specific educational material, and also company or office-wide video content, such as informational and promotional shots for a new program or directive.


### Use Case 3: Educational portal on the Web

For an educational portal as a product, you will be able to leverage the demographic structure of your audience to provide the most relevant courses at enhanched speeds. This can help you drive customer satisfaction and provide much better experience for your audience to help your product stand out against the competition. Maybe you will provide localized versions of course materials and provide them to your users at their locations, as a faster and more relevant alternative to other alternatives.

## Use Case 4: Real-time ad delivery service

Another possibility where the edge computing can bring you a competitive advantage is a real-time ad bidding and delivery service. This is an area where each millisecond counts, and so providing the ad content at faster rates than your competitors will be a huge advantage in your favour.

In the ad business, ads are provided to the client through an automated bidding service, where content providers compete against each other through automated bids. The winning bidder will then be chosen to provide ad content to the customer. Cutting off the latency-related milliseconds will give your platform the boost it needs to win in this sector by providing the content faster and allowing for the next bid to start faster.

## Wrapup

These are just a few use case possibilities representing areas where edge adoption will bring in concrete benefits. We only focused on video content delivery this time, but edge computing, and the shift it represents, will bring their benefits in many other areas as well.

Thank you for joining us for this short use cases ideation session, and as always, stay tuned for more!


